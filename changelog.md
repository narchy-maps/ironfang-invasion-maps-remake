# Ironfang Invasion Maps Remake

### 3.0.0

Book 3:

- A-IronfangDeserters.webp
- C-ForestParty.webp
- D-Patchy.webp
- E-BuletteAmbush.webp
- F-GriffonKill.webp
- G-TrapdoorSpider.webp
- H-RemainsOfRedburrow.webp
- I-RadyasHollowMine.webp
- J-RidgeCamp.webp


### 2.0.3

Book 2: 

k. Fort Nunder
l. Fort Trevalay

### 2.0.2

Book 2:

j.fortristin

## 2.0.1

Book 2:
c.hobgoblinpatrol
d.beetlewarren
e.burnedblind
f.pixiehunter
g.longfrondsdale
h.chimera
i.trollrejects
j.fortristin
a.owlbearattack
b.trappedlogger

## 1.0.0

Public release

### 0.0.5

- H2 Patrol
- I All Eyes Wood
- J Hunters Stead
- K Upper Caverns
- L Ruins
- M Camp Red

### 0.0.4

Updated Book 1 pack

### 0.0.3

Added:
- F2 Hermit
- F3 Bramble Wines
- G1 Wolf Lair
- G2 Tyranny
- G3 Hunting Party
- G4 Gristledown

### 0.0.2

- A Orelds' Fine Shop
- B Trading Company
- C Riverwood Shrine
- D Taproot Invasion
- E Bridge
- F1 Wasp Orchard

### 0.0.1

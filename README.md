# Ironfang Invasion Maps Remake

![Preview Image](preview/ironfang-invasion-preview.jpg)

This module is a collection of maps for the Iron Fang Invasion Adventure Path, published by Paizo for Pathfinder 1e. 

These maps are intended for use by the Pathfinder community, in their virtual tabletop environment of choice, to make setting up the maps a little bit easier. You will still need to purchase the [contents of the adventure from the Paizo Store](https://paizo.com/products/btpy9npj). 

A big thank you to Brianthas for his support on making these maps. 

So far, there are maps available for:

#### Book 1 - Trail of the Hunted
- Part 1
    - Area A, B, C, D, E
- Part 2
    - Area F1-3, G1-4, H2, I, J
- Part 3
    - Area K, L
- Part 4
    - Area M

#### Book 2 - Fangs of War
- Part 1
    - Area A, B, C, D, E, F, G, H, I,
- Part 2
    - Area J
- Part 3 
    - Area K
- Part 4
    - Area L

## Credits
[PAIZO](https://paizo.com/) - Some of the maps uses trademarks and/or copyrights owned by Paizo Inc., used under Paizo's Community Use Policy. We are expressly prohibited from charging you to use or access this content. The maps are not published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, visit paizo.com

[FORGOTTEN ADVENTURES](https://www.forgotten-adventures.net/info/) - Assets used to create this map are from Forgotten Adventures. These guys rock.

[NARCHY](https://ko-fi.com/narchymaps) - Hey, it's me! Some of the weirder assets are my own creations.

## Installing the module
